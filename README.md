# First installation
- Clone the repository: `git clone https://bitbucket.org/followupcio/website_dev_env`
- Pull repository's submodule: `make buildfao`
- Launch system: `make up`
- Restore the database using the `/database/empty_typo3_db.sql` dump

## MySQL configuration

Setting       | Value
------------- | -------------
User          | dev
Password      | dev 
Database      | typo3
Host          | 127.0.0.1
External Port | 13306

# Normal dev session 
- Launch system: `make up`
- Normally you will write your code in `/app/website`
- Once finished, remember to create a pull request to include your changes in the master branch [HOWTO](https://www.atlassian.com/git/tutorials/making-a-pull-request)

# Add typo3 plugins
[How to install Typo3 plugin with composer](https://composer.typo3.org/)

# Project requirements
- PHP 7.0+
- [PHP Composer](https://getcomposer.org)
- [Docker-compose](https://docs.docker.com/compose/install/) this will install docker too





# Original project

![TYPO3 Docker Boilerplate](https://static.webdevops.io/typo3-docker-boilerplate.svg)

[![latest v5.2.0-beta3](https://img.shields.io/badge/latest-v5.2.0_beta3-green.svg?style=flat)](https://github.com/webdevops/TYPO3-docker-boilerplate/releases/tag/5.2.0-beta3)
![License MIT](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)

## Table of contents

- [First steps / Installation and requirements](/documentation/INSTALL.md)
- [Updating docker boilerplate](/documentation/UPDATE.md)
- [Customizing](/documentation/CUSTOMIZE.md)
- [Services (Webserver, MySQL... Ports, Users, Passwords)](/documentation/SERVICES.md)
- [Docker Quickstart](/documentation/DOCKER-QUICKSTART.md)
- [Run your project](/documentation/DOCKER-STARTUP.md)
- [Container detail info](/documentation/DOCKER-INFO.md)
- [Troubleshooting](/documentation/TROUBLESHOOTING.md)
- [Changelog](/CHANGELOG.md)


Did I forget anyone? Send me a tweet or create pull request!
